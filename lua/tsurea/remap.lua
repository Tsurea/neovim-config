vim.g.mapleader = " "
vim.g.maplocalleader = " "

local keymap = vim.keymap

-- Escape from insert mode
keymap.set("i", "jk", "<Esc>") 

-- Delete single character without copying
keymap.set("n", "x", '"_x')
keymap.set("n", "s", '"_s')

-- Remap to explore mode
keymap.set("n", "<leader>pv", vim.cmd.Ex) 

keymap.set("n", "<leader>sv", "<C-w>v") -- split window vertically
keymap.set("n", "<leader>sh", "<C-w>s") -- split window horizontally
keymap.set("n", "<leader>se", "<C-w>=") -- make split windows equal width & height
keymap.set("n", "<leader>sx", ":close<CR>") -- close current split window

-- Move cursor in another window
keymap.set("n", "<C-h>", "<C-w>h>")
keymap.set("n", "<C-j>", "<C-w>j>")
keymap.set("n", "<C-k>", "<C-w>k>")
keymap.set("n", "<C-l>", "<C-w>l>")

keymap.set("n", "<leader>to", ":tabnew<CR>") -- open new tab
keymap.set("n", "<leader>tx", ":tabclose<CR>") -- close current tab
keymap.set("n", "<leader>tn", ":tabn<CR>") -- go to next tab
keymap.set("n", "<leader>tp", ":tabp<CR>") -- go to previous tab
